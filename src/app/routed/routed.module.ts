import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToComponentComponent } from './to-component/to-component.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: '', component: ToComponentComponent}
]

@NgModule({
  declarations: [ToComponentComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  providers: []

})
export class RoutedModule { }
