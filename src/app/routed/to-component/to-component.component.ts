import { Component, OnInit } from '@angular/core';
import {SharedService} from '../../shared.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-to-component',
  templateUrl: './to-component.component.html',
  styleUrls: ['./to-component.component.scss']
})
export class ToComponentComponent implements OnInit {

  public data: number[];
  constructor(private service: SharedService, private router: Router) { }

  ngOnInit() {
    this.data = this.service.getData();
    console.log('Recieved Data: ', this.data);
  }

}
