import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToComponentComponent } from './to-component.component';
import {RouterTestingModule} from '@angular/router/testing';

describe('ToComponentComponent', () => {
  let component: ToComponentComponent;
  let fixture: ComponentFixture<ToComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToComponentComponent ],
      imports: [
        RouterTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
