import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  public data: number[] = [];

  constructor() {
    console.log('Shared Service Created');
  }

  public getData(): number[] {
    return [...this.data];
  }
}
