import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FromComponentComponent } from './from-component.component';
import {RouterTestingModule} from '@angular/router/testing';

describe('FromComponentComponent', () => {
  let component: FromComponentComponent;
  let fixture: ComponentFixture<FromComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FromComponentComponent ],
      imports: [
        RouterTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FromComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
