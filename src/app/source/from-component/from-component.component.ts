import { Component, OnInit } from '@angular/core';
import {SharedService} from '../../shared.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-from-component',
  templateUrl: './from-component.component.html',
  styleUrls: ['./from-component.component.scss']
})
export class FromComponentComponent implements OnInit {

  constructor(private service: SharedService, private router: Router) {
  }

  ngOnInit() {
  }

  onNavigate() {
    this.service.data = [1, 2, 3, 4];
    this.router.navigate(['/to']);
  }
}
