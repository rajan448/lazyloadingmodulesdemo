import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FromComponentComponent} from './from-component/from-component.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedService} from '../shared.service';

const routes: Routes = [
  {path: '', component: FromComponentComponent}
];

@NgModule({
  declarations: [FromComponentComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  providers: []
})
export class SourceModule {
}
